  #032 Retirement Readiness Second Attempt


# 0) Validate lack of data for previous attempt  ---------------------------------------------------------------------
  
    #cont.5y <- subset(cont, date >= "2014-01-01" & date <= "2014-12-31")
    #conters <- data.frame(unique(cont.5y$id))
    #nrow(conters)/nrow(members)  

# 1) Calculate 'Balance Profile' ------------------------------------------

    mean.balance <- aggregate(members$Mbr_Acc_Bal_Amt, by = list(members$age), FUN = mean)
    colnames(mean.balance) <- c("age","balance")
    mean.balance <- subset(mean.balance, age >= 18 & age <= 65) #This implicitly places a restriction on model application to 18 or over
    mean.balance$growth <-  lead(mean.balance$balance)/mean.balance$balance        
    n <- nrow(mean.balance) -1
    for(i in 1:nrow(mean.balance)){
      strip <- data.frame(mean.balance$growtd[i:n])
      colnames(strip) <- "growth"
      mean.balance$growth.c[i] <- prod(strip$growth)
    }
    mean.balance$growth.c[48] <- 1
  
# 2) Estimated Retirement Amount ------------------------------------------
    
    #Calcualte ERA
    ERA <- subset(members, age >= 18 & age <= 65, select = c("Durable_Mbr_Acc_ID", "age", "Mbr_Acc_Bal_Amt"))
    colnames(ERA) <- c("id", "age", "balance")
    ERA <- merge(ERA, subset(mean.balance, select = c("age", "growth.c")), all.x = TRUE)
    ERA$ERA <- ERA$balance*ERA$growth.c   

# 3) Estimated Retirement Need --------------------------------------------
    
    #Find out who has a partner
    beneficiaries <- id %>%
      left_join(y = sqlQuery(channel = cn,  query = paste0(
        "SELECT ",
        "beneficiaries.Durable_Mbr_Acc_ID ",
        ", relationship.Relationship_Type_Desc ",
        ", beneficiaries.Current_Flag ",
        "FROM ",
        " dds.tfb_Mbr_Acc_Beneficiary AS beneficiaries ",
        " LEFT JOIN ",
        " dds.td_Beneficiary_Nomination_Type AS type ",
        " ON beneficiaries.Beneficiary_Nomination_Type_ID = type.Beneficiary_Nomination_Type_ID ",
        " LEFT JOIN ",
        " dds.td_Relationship_Type AS relationship ",
        " ON beneficiaries.Beneficiary_Relationship_Type_ID = relationship.Relationship_Type_ID " ,
        "WHERE ",
        " beneficiaries.Current_Flag > 0 "
      )
      ),
      by = "Durable_Mbr_Acc_ID"
      )
    beneficiaries$partner <- ifelse(beneficiaries$Relationship_Type_Desc == "Boyfriend"
                                    | beneficiaries$Relationship_Type_Desc == "Child"
                                    | beneficiaries$Relationship_Type_Desc == "Civil Partner"
                                    | beneficiaries$Relationship_Type_Desc == "Daughter"
                                    | beneficiaries$Relationship_Type_Desc == "Daughter In Law"
                                    | beneficiaries$Relationship_Type_Desc == "Defacto"
                                    | beneficiaries$Relationship_Type_Desc == "Fiance"
                                    | beneficiaries$Relationship_Type_Desc == "Fiancee"
                                    | beneficiaries$Relationship_Type_Desc == "Foster Child"
                                    | beneficiaries$Relationship_Type_Desc == "Foster Daughter"
                                    | beneficiaries$Relationship_Type_Desc == "Foster Son"
                                    | beneficiaries$Relationship_Type_Desc == "Girlfriend"
                                    | beneficiaries$Relationship_Type_Desc == "Granddaughter"
                                    | beneficiaries$Relationship_Type_Desc == "Grandson"
                                    | beneficiaries$Relationship_Type_Desc == "Husband"
                                    | beneficiaries$Relationship_Type_Desc == "Partner"
                                    | beneficiaries$Relationship_Type_Desc == "Son"
                                    | beneficiaries$Relationship_Type_Desc == "Son in Law"
                                    | beneficiaries$Relationship_Type_Desc == "Spouse"
                                    | beneficiaries$Relationship_Type_Desc == "Step Child"
                                    | beneficiaries$Relationship_Type_Desc == "Step Daughter"
                                    | beneficiaries$Relationship_Type_Desc == "Step Son"
                                    | beneficiaries$Relationship_Type_Desc == "Wife", "Yes", NA)
    #Merge in table into ERA
    colnames(beneficiaries)[1] <- "id"
    partners <- subset(beneficiaries, select = c("id", "partner"))
    partners <- data.frame(unique(partners[complete.cases(partners),]))
    ERA <- merge(ERA, partners, all.x = TRUE)
    ERA$partner[is.na(ERA$partner)] <- "No"    
    
    #Add gender to
    gender <- subset(members, select = c("Durable_Mbr_Acc_ID", "Gender_Type_Code"))
    colnames(gender) <- c("id", "gender")
    ERA <- merge(ERA, gender)
    
    #Specify model parameters
    i = 0.02 #inflation
    g = 0.06 #mean investment return
    n.m = 81 - 65 #male life expectancy less retirement
    n.f = 85 - 65 #female life expectancy less retirement
    n = 81 - 65 #non supplied life expectancy less retirement
    #This is based on the ASFA modest standard
    st.1 <- 27648
    st.2 <- 39775/2
    
    #Perform calculation
    ERA$retire.st <- ifelse(ERA$partner == "Yes", st.2, st.1)
    ERA$remaining.le <- ifelse(ERA$gender == "M", n.m, ifelse(ERA$gender == "F", n.f, n))
    ERA$remaining.wf <- 65 - ERA$age
    ERA$RN <- ERA$retire.st*(1+i)^ERA$remaining.wf*(1+g)*((1-((1+i)/(1+g))^ERA$remaining.le)/(g-i))
    
    #Finally, calculate retirement preparedness gap
    ERA$RPG <- pmax(1-ERA$ERA/ERA$RN,0)
    