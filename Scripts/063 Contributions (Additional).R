  ###Analysis for additional contributions

  #Designed to run after 062. As such, cont will be available.


# 1) Cleaning -------------------------------------------------------------

  #Remove SGs and contributions more than five years ago or in the future
  cont.add <- cont %>%
    filter(type == "Voluntary" | type == "Clever") %>%
    subset(date <= ymd("2019-02-28") & start.date > ymd("2014-02-28"))


  #Separate Tenure from the members table
  #NB: We deal with this now because some members have records of additional contributions before acc created date.
  tenure <- subset(members, select = c("Durable_Mbr_Acc_ID", "Mbr_Acc_Created_Date"))
  colnames(tenure) <- c("id", "acc.created")
  cont.add <- merge(cont.add, tenure, all.x = TRUE)
  cont.add$acc.created <- ymd(cont.add$acc.created)
  #cont.add$error <- ifelse(cont.add$acc.created <= cont.add$start.date, "no", "yes")
  #cont.add$error2 <- ifelse(cont.add$acc.created <= cont.add$date, NA, cont.add$acc.created - cont.add$date)
  #Below two lines hardcode account created date as first day of additional contributions coverage
  #cont.add$start.date[cont.add$start.date > cont.add$acc.created] <- cont.add$acc.created
  #cont.add$start.date[cont.add$start.date > cont.add$acc.created] <- cont.add$acc.created
  #ISSUE TO BE SORTED - TBC
  
  #Then calculate days since origin, which in this case is 2014-02-28
  origin = ymd("2014-02-28")
  cont.add$start.date.o <- difftime(cont.add$start.date, origin, units = "days")
  cont.add$date.o <- difftime(cont.add$date, origin, units = "days")  
  rm(origin)
  
  #Then calculate sum of weights between these days
  cont.add$date.o <- as.numeric(cont.add$date.o)
  cont.add$start.date.o <- as.numeric(cont.add$start.date.o)
  cont.add$coverage <- (cont.add$date.o-cont.add$start.date.o+1)*(cont.add$date.o+cont.add$start.date.o)/2
  
  #Assign weights to the contributions
  cont.add$weight <- ifelse(cont.add$type == "Clever", 2, 1)
  #Multiply weight by coverage
  cont.add$score <- cont.add$coverage*cont.add$weight
  #Sum up scores for each member
  
  #cont.add.score <- aggregate(cont.add$score, by = list(cont.add$id), FUN = sum)
  #colnames(cont.add.score) <- c("id", "score")
  
  #Tenure analysis
  tenure <- subset(members, select = c("Durable_Mbr_Acc_ID", "Mbr_Acc_Created_Date"))
  colnames(tenure) <- c("id", "acc.created")
  #Hardcode floor value
  tenure$acc.created[tenure$acc.created <= "2014-02-28"] <- "2014-03-01"
  #Hardcode ceiling value
  tenure$acc.created[tenure$acc.created >= "2019-02-28"] <- "2019-02-28"
  tenure$acc.created.o <- ceiling(difftime(tenure$acc.created, ymd("2014-02-28"), units = "days"))
  #Calculate coverage for tenure
  tenure$acc.created.o <- as.numeric(tenure$acc.created.o)
  
  #tenure$tenure.c <- (1826+1-tenure$acc.created.o)*(tenure$acc.created.o+1826)/2
  
  #Merge into the other table
  cont.add.score <- merge(cont.add.score, subset(tenure, select = c("id","tenure.c")), by = "id", all.x = TRUE)
  rm(tenure)
  #Divide score by tenure to get weighted score
  cont.add.score$w.score <- cont.add.score$score/cont.add.score$tenure.c
  rm(origin)