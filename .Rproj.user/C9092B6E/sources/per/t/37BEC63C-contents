# 010 LIFE STAGE BENEFICIARIES

# This script determines life stage by the member's beneficiaries


# 0) PREPARE ENVIRONMENT
    #Load libraries
    library(lubridate) # Date packages    
    library(tidyverse) # Hadley Wickham's packages
    library(here) # Easier relative paths
    library(RODBC) # Connect to SQL

    #Check file path
    getwd()

# 1) SET UP CONNECTION ---------------------------------------------------------

    #SQL Server connection string
    cn <- odbcDriverConnect( 
        connection = paste0("Driver={SQL Server Native Client 11.0};" 
                            , "server=BISQLDEV;"
                            , "database=CBUS_DATA_STORE;"
                            , "trusted_connection=yes;")
    )

    id <- get_active_members()

# 2) GET LIFE STAGE DATA -----------------------------------------------------
    
    beneficiaries <- id %>%
        left_join(y = sqlQuery(channel = cn,  query = paste0(
            "SELECT ",
            "beneficiaries.Durable_Mbr_Acc_ID ",
            ", relationship.Relationship_Type_Desc ",
            ", beneficiaries.Current_Flag ",
            "FROM ",
            " dds.tfb_Mbr_Acc_Beneficiary AS beneficiaries ",
            " LEFT JOIN ",
            " dds.td_Beneficiary_Nomination_Type AS type ",
            " ON beneficiaries.Beneficiary_Nomination_Type_ID = type.Beneficiary_Nomination_Type_ID ",
            " LEFT JOIN ",
            " dds.td_Relationship_Type AS relationship ",
            " ON beneficiaries.Beneficiary_Relationship_Type_ID = relationship.Relationship_Type_ID " ,
            "WHERE ",
            " beneficiaries.Current_Flag > 0 "
        )
        ),
        by = "Durable_Mbr_Acc_ID"
        ) %>%
    mutate(generation_number = ifelse(
        Relationship_Type_Desc %in% c("Brother", "Sister",
                                      "Foster Brother", 
                                      "Foster Sister",
                                      "Cousin",
                                      "Half Brother", 
                                      "Half Sister",  
                                      "Sibling", "Step Brother",
                                      "Step Sister",
                                      
                                      "Flat mate", "Employer",
                                      
                                      "Mother", "Father",
                                      "Parent",
                                      "Foster Parent",
                                      # "Partner", 
                                      "Guardian",
                                      "Step Father", "Step Mother",
                                      "Step Parent",
                                      "God Parent",
                                      
                                      "Aunt", "Uncle",
                                      
                                      "Grandfather", "Grandmother",
                                      "Grandparent"
        ), 1,
        ifelse(
            Relationship_Type_Desc %in% c("Wife", "Husband", 
                                          "Boyfriend", "Girlfriend",
                                          "Friend",
                                          "Civil Partner", "Defacto",
                                          "Father in Law", "Mother in Law",
                                          "Fiance",
                                          "Fiancee", "In Laws",
                                          "Interdependent", "Partner",
                                          "Spouse"
            ), 2,
            ifelse(Relationship_Type_Desc %in% c("Son", "Daughter",
                                                 "Dependant", "Child",
                                                 "Brother In Law",
                                                 "Sister In Law",
                                                 "Sister in Law",
                                                 "Ex-Defacto",
                                                 "Ex-Husband",
                                                 "Ex-Partner",
                                                 "Ex-Spouse",
                                                 "Ex-Nuptial Daughter",
                                                 "Ex-Nuptial Son",
                                                 "Ex-Partner",
                                                 "Ex-Wife",
                                                 "Family", 
                                                 "Financial Dependent",
                                                 "Foster Child",
                                                 "Foster Daughter",
                                                 "Foster Son",
                                                 "Godchild",
                                                 "Niece", "Nephew",
                                                 "Step Child",
                                                 "Step Daughter", 
                                                 "Step Niece", 
                                                 "Step Nephew",
                                                 "Step Son"
            ), 3,
            ifelse(Relationship_Type_Desc %in% c("Grand child",
                                                 "Grandson",
                                                 "Granddaughter",
                                                 "Daughter In Law",
                                                 "Son in Law", 
                                                 "Estate", 
                                                 "Executor", 
                                                 "Legal Rep"
            ), 
            4, 
            ifelse(Relationship_Type_Desc %in% c("Other", "Relative",
                                                 "Unknown", "Unmatched"
            ),
            -1,
            0
            ))
            )
        )
    )) %>%
    select(Durable_Mbr_Acc_ID, Current_Flag, generation_number) %>% #989,178
    mutate(Current_Flag = replace_na(Current_Flag, 0)) %>%
    group_by(Durable_Mbr_Acc_ID) %>% #2,068,623 rows
    summarise(Current_Flag = max(Current_Flag), 
              generation_number = max(generation_number)
              ) %>%
    mutate(generation = recode_factor(generation_number, 
                                      `-1` = "Other", 
                                      `0` = "Missing", 
                                      `1` = "Single", 
                                      `2` = "Couple", 
                                      `3`= "Parent", 
                                      `4` = "Grandparent"
                                      )) %>%
        select(Durable_Mbr_Acc_ID,
               has_beneficiary = Current_Flag,
               generation_number,
               generation               
               )
        
        
    
        

    # Visualise table ----
    epiDisplay::tab1(a$generation, 
                     sort.group = FALSE, 
                     cum.percent = FALSE,
                     main = "CBUS Member Life Stage, (inferred from their nominated beneficiary)"
    )   
    prop.table(table(a$generation)) * 100
    
    
    #setdiff(beneficiaries$Durable_Mbr_Acc_ID, id$Durable_Mbr_Acc_ID)
        
    
    #glimpse(summarise_all( members, funs(sum(is.na))))
    #all(id$Durable_Mbr_Acc_ID == beneficiaries$Durable_Mbr_Acc_IDrable_Mbr_Acc_ID)
    